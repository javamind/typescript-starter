import {GreaterService, Person} from "../../main/typescript/person";

describe('Test person.ts', () => {
    let service: GreaterService;

    beforeEach(() => service = new GreaterService());

    test('should say', () => {
        const person: Person = {
            firstName: 'Guillaume',
            lastName: 'EHRET'
        };
        expect(service.great(person)).toBe('Hi, Guillaume EHRET');
    })
});