export interface Person {
    firstName: string;
    lastName: string;
}

export class GreaterService {

    great(person: Person){
        return `Hi, ${person.firstName} ${person.lastName}`;
    }

//     t(){
//     console.log(this.great(123));
//     let age = 42;
//     age = "inconnu";
// }

}
